# Azure Agent Ubuntu 18.04 or 20.04

## Contains

- .NET Core 5.0
- Mono Stable development

## Build

```
docker build -t dockeragent -f Dockerfile-ubuntu-18.04 .
```
or
```
docker build -t dockeragent -f Dockerfile-ubuntu-20.04 .
```

## Before run
Create a PAT token in your azure devops instance.

## Run

```
docker run -e AZP_URL=<Azure DevOps instance> -e AZP_TOKEN=<PAT token> -e AZP_AGENT_NAME=mydockeragent dockeragent:18.04
```

## References

- [Self-Hosted Agents](https://docs.microsoft.com/en-us/azure/devops/pipelines/agents/docker?view=azure-devops)
- [PAT Token](https://docs.microsoft.com/en-us/azure/devops/organizations/accounts/use-personal-access-tokens-to-authenticate?view=azure-devops&tabs=preview-page)
